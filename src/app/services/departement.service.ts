import {Injectable} from '@angular/core';
import {Departement} from '../classes/departement';
import {Observable, of} from 'rxjs';

let DEPARTEMENTS = [
  {id: 1, name: 'Administration'},
  {id: 2, name: 'Ressources Humaines'},
  {id: 3, name: 'Comptabilité'},
  {id: 4, name: 'Digital'},
  {id: 5, name: 'Marketing'},
];
let DEPARTEMENTS_SEQ = 6;

@Injectable({
  providedIn: 'root'
})
export class DepartementService {

  constructor() {
  }

  all(): Observable<Departement[]> {
    return of(DEPARTEMENTS);
  }

  add(nom: string) {
    DEPARTEMENTS.push({id: DEPARTEMENTS_SEQ, name: nom});
    DEPARTEMENTS_SEQ++;
  }

  findById(id): Departement {
    return DEPARTEMENTS.filter((dept) => {
      // tslint:disable-next-line:triple-equals
      return dept.id === id;
    })[0];
  }

  update(id, nom: string) {
    DEPARTEMENTS = DEPARTEMENTS.filter((dept) => {
      return dept.id !== id;
    });
    DEPARTEMENTS.push({id, name: nom});
  }
}
