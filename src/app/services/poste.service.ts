import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Poste} from '../classes/poste';
import {Departement} from '../classes/departement';
import {DepartementService} from './departement.service';

let POSTES = [
  {id: 1, name: 'Développeur Junior', departement: {id: 4, name: 'Digital'}},
  {id: 2, name: 'Développeur Senior', departement: {id: 4, name: 'Digital'}},
  {id: 3, name: 'Comptable Junior', departement: {id: 3, name: 'Comptabilité'}},
  {id: 4, name: 'Comptable Senior', departement: {id: 3, name: 'Comptabilité'}},
  {id: 5, name: 'Auditeur Junior', departement: {id: 3, name: 'Comptabilité'}},
  {id: 6, name: 'Auditeur Senior', departement: {id: 3, name: 'Comptabilité'}},
  {id: 7, name: 'Responsable', departement: {id: 2, name: 'Ressources Humaines'}},
  {id: 8, name: 'Responsable', departement: {id: 5, name: 'Marketing'}},
  {id: 9, name: 'Assistant', departement: {id: 5, name: 'Marketing'}},
  {id: 10, name: 'Administrateur', departement: {id: 1, name: 'Administration'}},
  {id: 11, name: 'Secrétaire', departement: {id: 1, name: 'Administration'}},
];

let POSTES_SEQ = 12;

@Injectable({
  providedIn: 'root'
})
export class PosteService {

  constructor(private departementService: DepartementService) {
  }

  all(): Observable<Poste[]> {
    return of(POSTES);
  }

  add(nom: string, selectedDept: Departement) {
    const items = {id: POSTES_SEQ, name: nom, departement: this.departementService.findById(selectedDept.id)};
    POSTES.push(items);
    POSTES_SEQ++;
  }

  update(id, selectedDept: Departement, nom: string) {
    POSTES = POSTES.filter((poste) => {
      return poste.id !== id;
    });
    console.log(selectedDept);
    console.log(POSTES);
    POSTES.push({id, name: nom, departement: this.departementService.findById(selectedDept.id)});
  }
}
