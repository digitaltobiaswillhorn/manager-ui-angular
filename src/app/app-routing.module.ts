import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {NoTopbarComponent} from './layouts/no-topbar/no-topbar.component';
import {LoginComponent} from './pages/login/login.component';
import {RegistrationComponent} from './pages/registration/registration.component';
import {WithTopbarComponent} from './layouts/with-topbar/with-topbar.component';
import {AdministrationComponent} from './layouts/administration/administration.component';
import {UsersComponent} from './pages/users/users.component';
import {AchatComponent} from './layouts/achat/achat.component';
import {MatieresPremieresComponent} from './pages/matieres-premieres/matieres-premieres.component';
import {ParametrageComponent} from './layouts/parametrage/parametrage.component';
import {VenteComponent} from './layouts/vente/vente.component';
import {StockComponent} from './layouts/stock/stock.component';
import {DepartementsComponent} from './pages/departements/departements.component';
import {PostesComponent} from './pages/postes/postes.component';
import {AccessComponent} from './pages/access/access.component';
import {DepartementsPostesComponent} from './pages/departements-postes/departements-postes.component';

const routes: Routes = [
  {
    path: 'signin', component: NoTopbarComponent, children: [
      {path: '', component: LoginComponent},
      {path: 'new', component: RegistrationComponent}
    ]
  },
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'administration', redirectTo: '/administration/departements-postes', pathMatch: 'full'},
  {
    path: '', component: WithTopbarComponent, children: [
      {path: 'dashboard', component: DashboardComponent},
      {
        path: 'administration', component: AdministrationComponent, children: [
          {path: 'departements-postes', component: DepartementsPostesComponent},
          {path: 'access', component: AccessComponent, children: []}
        ]
      },
      {
        path: 'achat', component: AchatComponent, children: [
          {path: 'matieres-premieres', component: MatieresPremieresComponent}
        ]
      },
      {path: 'employes', component: UsersComponent, children: []},
      {path: 'parametrage', component: ParametrageComponent, children: []},
      {path: 'vente', component: VenteComponent, children: []},
      {path: 'stock', component: StockComponent, children: []},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
