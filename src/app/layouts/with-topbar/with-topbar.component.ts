import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { IpcRenderer } from 'electron';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-with-topbar',
  templateUrl: './with-topbar.component.html',
  styleUrls: ['./with-topbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WithTopbarComponent implements OnInit {

  private ipc: IpcRenderer;
  public environment = environment;

  constructor() {
    if ((window as any).require) {
      try {
        this.ipc = (window as any).require('electron').ipcRenderer;
        console.log(this.ipc);
      } catch (e) {
        throw e;
      }
    } else {
      console.warn('App not running inside Electron!');
    }
  }

  ngOnInit(): void {
  }

  closeWindow(): void {
    this.ipc.send('closeWindow');
  }

  maximizeWindow() {
    this.ipc.send('maximizeWindow');
  }

  minimizeWindow() {
    this.ipc.send('minimizeWindow');
  }
}
