import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithTopbarComponent } from './with-topbar.component';

describe('WithTopbarComponent', () => {
  let component: WithTopbarComponent;
  let fixture: ComponentFixture<WithTopbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WithTopbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithTopbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
