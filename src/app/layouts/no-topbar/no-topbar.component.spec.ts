import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoTopbarComponent } from './no-topbar.component';

describe('NoTopbarComponent', () => {
  let component: NoTopbarComponent;
  let fixture: ComponentFixture<NoTopbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoTopbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoTopbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
