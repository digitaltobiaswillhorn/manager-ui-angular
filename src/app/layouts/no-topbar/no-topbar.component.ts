import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-topbar',
  templateUrl: './no-topbar.component.html',
  styleUrls: ['./no-topbar.component.scss']
})
export class NoTopbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
