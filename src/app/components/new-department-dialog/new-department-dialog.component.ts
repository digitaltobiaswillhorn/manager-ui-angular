import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-new-department-dialog',
  templateUrl: './new-department-dialog.component.html',
  styleUrls: ['./new-department-dialog.component.scss']
})
export class NewDepartmentDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<NewDepartmentDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
