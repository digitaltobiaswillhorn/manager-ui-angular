import { Component, OnInit } from '@angular/core';
import {Chart} from 'chart.js';

@Component({
  selector: 'app-achats-stats',
  templateUrl: './achats-stats.component.html',
  styleUrls: ['./achats-stats.component.scss']
})
export class AchatsStatsComponent implements OnInit {
  linechart;

  constructor() { }

  ngOnInit(): void {
    this.linechart = new Chart('achats-canvas', {
      type: 'line',
      data: {
        labels: ['Jan.', 'Fév.', 'Mars', 'Avr.', 'Mai', 'Jun.', 'Jul.', 'Août', 'Spt.', 'Oct.'],
        datasets: [
          {
            data: [16, 12, 26, 7, 5, 8, 11, 4, 25, 31],
            borderColor: '#673ab7',
            label: 'Demandes',
            fill: false
          }
        ]
      }
    });
  }

}
