import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AchatsStatsComponent } from './achats-stats.component';

describe('AchatsStatsComponent', () => {
  let component: AchatsStatsComponent;
  let fixture: ComponentFixture<AchatsStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchatsStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AchatsStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
