import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Departement} from '../../classes/departement';
import {DepartementService} from '../../services/departement.service';

@Component({
  selector: 'app-new-poste-dialog',
  templateUrl: './new-poste-dialog.component.html',
  styleUrls: ['./new-poste-dialog.component.scss']
})
export class NewPosteDialogComponent implements OnInit {
  departments: Departement[];

  constructor(public dialogRef: MatDialogRef<NewPosteDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private departementService: DepartementService) {
    console.log(data);
  }

  ngOnInit(): void {
    this.departementService.all().subscribe(depts => this.departments = depts);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
