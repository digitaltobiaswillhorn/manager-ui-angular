import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPosteDialogComponent } from './new-poste-dialog.component';

describe('NewPosteDialogComponent', () => {
  let component: NewPosteDialogComponent;
  let fixture: ComponentFixture<NewPosteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPosteDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPosteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
