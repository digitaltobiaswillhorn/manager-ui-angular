import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployesStatsComponent } from './employes-stats.component';

describe('EmployesStatsComponent', () => {
  let component: EmployesStatsComponent;
  let fixture: ComponentFixture<EmployesStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployesStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployesStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
