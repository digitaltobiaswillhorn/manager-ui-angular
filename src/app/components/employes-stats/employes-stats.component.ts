import {Component, OnInit} from '@angular/core';
import {Chart} from 'chart.js';
import {LineStat} from '../../classes/line-stat';
import {MatDialog} from '@angular/material/dialog';
import {NewEmployesDialogComponent} from '../new-employes-dialog/new-employes-dialog.component';

@Component({
  selector: 'app-employes-stats',
  templateUrl: './employes-stats.component.html',
  styleUrls: ['./employes-stats.component.scss']
})
export class EmployesStatsComponent implements OnInit {
  linechart;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.linechart = new Chart('emps-canvas', {
      type: 'bar',
      data: {
        labels: ['Jan.', 'Fév.', 'Mars', 'Avr.', 'Mai', 'Jun.', 'Jul.', 'Août', 'Spt.', 'Oct.'],
        datasets: [
          {
            data: [7, 3, 1, 12, 0, 1, 0, 3, 14, 5],
            borderColor: '#673ab7',
            backgroundColor: '#b388ff',
            borderWidth: 2,
            label: 'Nouveau'
          }
        ]
      }
    });
  }

  openNewEmployesDialog() {
    const dialogRef = this.dialog.open(NewEmployesDialogComponent, {
      width: '800px'
    });
  }
}
