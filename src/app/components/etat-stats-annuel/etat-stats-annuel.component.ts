import {Component, OnInit} from '@angular/core';
import {Chart} from 'chart.js';

@Component({
  selector: 'app-etat-stats-annuel',
  templateUrl: './etat-stats-annuel.component.html',
  styleUrls: ['./etat-stats-annuel.component.scss']
})
export class EtatStatsAnnuelComponent implements OnInit {
  linechart;

  constructor() {
  }

  ngOnInit(): void {
    this.linechart = new Chart('etat-annuel-canvas', {
      type: 'line',
      data: {
        labels: ['Jan.', 'Fév.', 'Mars', 'Avr.', 'Mai', 'Jun.', 'Jul.', 'Août', 'Spt.', 'Oct.'],
        datasets: [
          {
            data: [782000, 412000, 805600, 613400, 558200, 1005010, 322000, 453080, 144000, 256000],
            borderColor: '#673ab7',
            backgroundColor: '#9575cd',
            label: 'Dépenses',
            fill: true
          },
          {
            data: [1200590, 560500, 1040000, 656450, 345300, 860980, 786020, 946850, 682150, 1045000],
            borderColor: '#ffc107',
            backgroundColor: '#ffd54f',
            label: 'Recettes',
            fill: true
          }
        ]
      }
    });
  }

}
