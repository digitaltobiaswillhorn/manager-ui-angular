import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtatStatsAnnuelComponent } from './etat-stats-annuel.component';

describe('EtatStatsAnnuelComponent', () => {
  let component: EtatStatsAnnuelComponent;
  let fixture: ComponentFixture<EtatStatsAnnuelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtatStatsAnnuelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtatStatsAnnuelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
