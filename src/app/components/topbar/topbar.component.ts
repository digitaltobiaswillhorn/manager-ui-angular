import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
