import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentesStatsComponent } from './ventes-stats.component';

describe('VentesStatsComponent', () => {
  let component: VentesStatsComponent;
  let fixture: ComponentFixture<VentesStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentesStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentesStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
