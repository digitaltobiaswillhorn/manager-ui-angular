import {Component, OnInit} from '@angular/core';
import {Chart} from 'chart.js';

@Component({
  selector: 'app-ventes-stats',
  templateUrl: './ventes-stats.component.html',
  styleUrls: ['./ventes-stats.component.scss']
})
export class VentesStatsComponent implements OnInit {
  linechart;

  constructor() {
  }

  ngOnInit(): void {
    this.linechart = new Chart('ventes-canvas', {
      type: 'polarArea',
      data: {
        labels: ['Riz blanc', 'Riz basmati', 'Maïs Q1', 'Maïs Q2', 'Arachide'],
        datasets: [
          {
            data: [60, 35, 65, 80, 30],
            borderColor: ['#673ab7', '#4caf50', '#f44336', '#009688', '#ff9800'],
            backgroundColor: ['#673ab7', '#4caf50', '#f44336', '#009688', '#ff9800'],
            label: 'Nouveau',
            fill: false
          }
        ]
      },
      options: {
        animation: {
          animateRotate: false,
          animateScale: true
        }
      }
    });
  }

}
