import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEmployesDialogComponent } from './new-employes-dialog.component';

describe('NewEmployesDialogComponent', () => {
  let component: NewEmployesDialogComponent;
  let fixture: ComponentFixture<NewEmployesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewEmployesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEmployesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
