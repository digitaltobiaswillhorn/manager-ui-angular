import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-new-employes-dialog',
  templateUrl: './new-employes-dialog.component.html',
  styleUrls: ['./new-employes-dialog.component.scss']
})
export class NewEmployesDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<NewEmployesDialogComponent>) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
