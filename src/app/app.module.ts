import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import bootstrap from 'bootstrap';

import {AppComponent} from './app.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {TopbarComponent} from './components/topbar/topbar.component';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSidenavModule} from '@angular/material/sidenav';
import {LoginComponent} from './pages/login/login.component';
import {NoTopbarComponent} from './layouts/no-topbar/no-topbar.component';
import {RegistrationComponent} from './pages/registration/registration.component';
import {WithTopbarComponent} from './layouts/with-topbar/with-topbar.component';
import {AdministrationComponent} from './layouts/administration/administration.component';
import {AchatComponent} from './layouts/achat/achat.component';
import {VenteComponent} from './layouts/vente/vente.component';
import {ParametrageComponent} from './layouts/parametrage/parametrage.component';
import {UsersComponent} from './pages/users/users.component';
import {MatieresPremieresComponent} from './pages/matieres-premieres/matieres-premieres.component';
import {StockComponent} from './layouts/stock/stock.component';
import {DepartementsComponent} from './pages/departements/departements.component';
import {PostesComponent} from './pages/postes/postes.component';
import {AccessComponent} from './pages/access/access.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {EmployesStatsComponent} from './components/employes-stats/employes-stats.component';
import {AchatsStatsComponent} from './components/achats-stats/achats-stats.component';
import {EtatStatsAnnuelComponent} from './components/etat-stats-annuel/etat-stats-annuel.component';
import {VentesStatsComponent} from './components/ventes-stats/ventes-stats.component';
import {NewEmployesDialogComponent} from './components/new-employes-dialog/new-employes-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {DepartementsPostesComponent} from './pages/departements-postes/departements-postes.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatMenuModule} from '@angular/material/menu';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NewDepartmentDialogComponent} from './components/new-department-dialog/new-department-dialog.component';
import {NewPosteDialogComponent} from './components/new-poste-dialog/new-poste-dialog.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TopbarComponent,
    LoginComponent,
    NoTopbarComponent,
    RegistrationComponent,
    WithTopbarComponent,
    AdministrationComponent,
    AchatComponent,
    VenteComponent,
    ParametrageComponent,
    UsersComponent,
    MatieresPremieresComponent,
    StockComponent,
    DepartementsComponent,
    PostesComponent,
    AccessComponent,
    EmployesStatsComponent,
    AchatsStatsComponent,
    EtatStatsAnnuelComponent,
    VentesStatsComponent,
    NewEmployesDialogComponent,
    DepartementsPostesComponent,
    NewDepartmentDialogComponent,
    NewPosteDialogComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatTabsModule,
    MatSidenavModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatListModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatMenuModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatSelectModule
  ],
  entryComponents: [
    NewDepartmentDialogComponent,
    NewPosteDialogComponent,
    NewEmployesDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
