import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatieresPremieresComponent } from './matieres-premieres.component';

describe('MatieresPremieresComponent', () => {
  let component: MatieresPremieresComponent;
  let fixture: ComponentFixture<MatieresPremieresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatieresPremieresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatieresPremieresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
