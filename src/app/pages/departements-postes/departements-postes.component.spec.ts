import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartementsPostesComponent } from './departements-postes.component';

describe('DepartementsPostesComponent', () => {
  let component: DepartementsPostesComponent;
  let fixture: ComponentFixture<DepartementsPostesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartementsPostesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartementsPostesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
