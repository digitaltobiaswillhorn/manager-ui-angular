import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {User} from '../../classes/user';
import {Departement} from '../../classes/departement';
import {Poste} from '../../classes/poste';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {NewDepartmentDialogComponent} from '../../components/new-department-dialog/new-department-dialog.component';
import {NewPosteDialogComponent} from '../../components/new-poste-dialog/new-poste-dialog.component';
import {DepartementService} from '../../services/departement.service';
import {PosteService} from '../../services/poste.service';

@Component({
  selector: 'app-departements-postes',
  templateUrl: './departements-postes.component.html',
  styleUrls: ['./departements-postes.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DepartementsPostesComponent implements OnInit {
  usersData: User[];
  departmentsData: Departement[];
  postesData: Poste[];
  columnsToDisplay = ['id', 'email', 'name', 'lastname'];
  columnsToDisplayDept = ['id', 'name'];
  columnsToDisplayDeptRow = ['id', 'name', 'action'];
  columnsToDisplayPoste = ['id', 'dept', 'name', 'action'];
  usersDataSource: MatTableDataSource<User>;
  departmentsDataSource: MatTableDataSource<Departement>;
  postesDataSource: MatTableDataSource<Poste>;

  selectedDepartment: Departement;

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('deptPaginator') deptPaginator: MatPaginator;

  constructor(public dialog: MatDialog, private departementService: DepartementService, private posteService: PosteService) {
  }

  ngOnInit(): void {
    // this.usersData = [];
    // this.usersData.push(new User(1, 'baliaka@tobiaswillhorn.com', 'Baliaka', 'ANDRIATIANA'));
    // this.usersData.push(new User(2, 'tamby@tobiaswillhorn.com', 'Tamby', 'ANDRIATIANA'));
    // this.usersData.push(new User(3, 'iryah@tobiaswillhorn.com', 'Iryah', 'RANDRIAVELONTSOA'));
    // this.usersData.push(new User(4, 'miora@tobiaswillhorn.com', 'Miora', 'ANDRIATIANA'));
    // this.usersData.push(new User(5, 'maeva@tobiaswillhorn.com', 'Maeva', 'ANDRIATIANA'));

    this.usersDataSource = new MatTableDataSource<User>(this.usersData);
    this.departmentsDataSource = new MatTableDataSource<Departement>(this.departmentsData);
    this.postesDataSource = new MatTableDataSource<Poste>(this.postesData);

    this.fetchingDepartments();
    this.fetchingPostes();
  }

  private fetchingPostes() {
    this.posteService.all().subscribe(postes => {
      this.postesData = postes;
      this.postesDataSource.data = this.postesData;
    });
  }

  private fetchingDepartments() {
    this.departementService.all().subscribe(departements => {
      this.departmentsData = departements;
      this.departmentsDataSource.data = this.departmentsData;
    });
  }

  getPostes(deptData: Departement) {
    this.selectedDepartment = deptData;
    this.postesDataSource.data = this.postesData.filter((poste) => {
      return poste.departement.id === deptData.id;
    });
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit(): void {
    this.departmentsDataSource.paginator = this.deptPaginator;
    this.postesDataSource.paginator = this.paginator;
  }

  showAllDept() {
    this.postesDataSource.data = this.postesData;
    this.selectedDepartment = undefined;
  }

  openNewDeptDialog() {
    const dialogRef = this.dialog.open(NewDepartmentDialogComponent, {
      width: '400px',
      data: {
        edited: false
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.departementService.add(data.nom);
      }
      this.fetchingDepartments();
    });
  }

  openNewPosteDialog() {
    const dialogRef = this.dialog.open(NewPosteDialogComponent, {
      width: '500px',
      data: {
        selectedDept: {id: 0},
        edited: false
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.posteService.add(data.nom, data.selectedDept);
      }
      this.fetchingPostes();
    });
  }

  modifyDepartement(dept: Departement) {
    console.log('departement:');
    console.log(dept);
    const dialogRef = this.dialog.open(NewDepartmentDialogComponent, {
      width: '400px',
      data: {
        nom: dept.name,
        selectedDept: dept,
        edited: true
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.departementService.update(dept.id, data.nom);
        this.fetchingDepartments();
      }
    });
  }

  modifyPoste(poste: Poste) {
    const dialogRef = this.dialog.open(NewPosteDialogComponent, {
      width: '500px',
      data: {
        nom: poste.name,
        selectedDept: poste.departement,
        edited: true
      }
    });
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.posteService.update(poste.id, data.selectedDept, data.nom);
        this.fetchingPostes();
      }
    });
  }

  deletePoste(poste: Poste) {

  }

  deleteDepartement(dept: Departement) {

  }
}
