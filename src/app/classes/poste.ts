import {Departement} from './departement';

export class Poste {
  id;
  name;
  departement: Departement;


  constructor(id, name, departement: Departement) {
    this.id = id;
    this.name = name;
    this.departement = departement;
  }
}
