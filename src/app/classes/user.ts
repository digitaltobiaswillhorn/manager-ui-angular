export class User {
  id;
  email;
  name;
  lastname;


  constructor(id, email, name, lastname) {
    this.id = id;
    this.email = email;
    this.name = name;
    this.lastname = lastname;
  }
}
